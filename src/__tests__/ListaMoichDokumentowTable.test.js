import React from "react";
import { createMount } from "@material-ui/core/test-utils";
import ListaMoichDokumentowTable from "./../Components/Dokumenty/MojeDokumenty/ListaMoichDokumentowTable";

describe("ListaMoichDokumentowTable Component", () => {
  let mount;

  const props = {
    documents: [
      {
        documentId: 2,
        documentTypeName: "Warunki",
        fileList: ["bilans-kompetencji_nr-albumu.doc"],
        sendTime: "2019-06-05T17:36:59.4608305",
        status: "Awaiting",
        title: "dasasdasdasd"
      },
      {
        documentId: 3,
        documentTypeName: "Warunki",
        fileList: ["bilans-kompetencji_nr-albumu.doc"],
        sendTime: "2019-06-05T17:36:59.4608305",
        status: "Awaiting",
        title: "dasasdasdasd"
      },
      {
        documentId: 1,
        documentTypeName: "Warunki",
        fileList: ["bilans-kompetencji_nr-albumu.doc"],
        sendTime: "2019-06-05T17:36:59.4608305",
        status: "Awaiting",
        title: "dasasdasdasd"
      }
    ],
    orderBy: "Id",
    orderType: "desc"
  };

  beforeEach(() => {
    mount = createMount();
  });

  it("Should render appropriate number of columns in table", () => {
    const component = mount(<ListaMoichDokumentowTable {...props} />);

    // console.log(component.debug());
    const wrapper = component.find("th");
    expect(wrapper.length).toBe(5);
  });

  it("Should render appropriate number of objects from props in table", () => {
    const component = mount(<ListaMoichDokumentowTable {...props} />);

    // console.log(component.debug());
    const wrapper = component.find("tr");
    expect(wrapper.length).toBe(3 + 1); // 3 obiekty + 1 headery
  });
});
