import React from "react";
import { shallow, mount } from "enzyme";
import App from "./../App";
import { findByTestAttr } from "./../Helpers/utils_tests";
import WyswietlDokumentBox from "../Components/Dokumenty/WyswietlanieSzczegolowe/WyswietlDokumentBox";
import { Fragment, Paper } from "react";
import ListaMoichDokumentowTable from "./../Components/Dokumenty/MojeDokumenty/ListaMoichDokumentowTable";

// test();
// describe() tylko opisuje testy w srodku

const setUp = (props = {}) => {
  const component = shallow(<App {...props} />);
  return component;
};

describe("App.js Component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("Should render HeaderSuperAdmin Component if Superadmin is logged in", () => {
    component.setState({ loggedInUserRole: "Superadmin" });
    const wrapper = findByTestAttr(component, "headerSuperAdmin");
    expect(wrapper.length).toBe(1);
  });

  it("Should render Header Component if Student is logged in", () => {
    component.setState({ loggedInUserRole: "Student" });
    const wrapper = findByTestAttr(component, "header");
    expect(wrapper.length).toBe(1);
  });

  it("Should render Header Component if no user is logged in", () => {
    component.setState({ loggedInUserRole: undefined });
    const wrapper = findByTestAttr(component, "header");
    expect(wrapper.length).toBe(1);
  });

  it("Should render HeaderModerator Component if Moderator is logged in", () => {
    component.setState({ loggedInUserRole: "Moderator" });
    const wrapper = findByTestAttr(component, "headerModerator");
    expect(wrapper.length).toBe(1);
  });

  it("Should render HeaderAdministrator Component if Admin is logged in", () => {
    component.setState({ loggedInUserRole: "Admin" });
    const wrapper = findByTestAttr(component, "headerAdministrator");
    expect(wrapper.length).toBe(1);
  });

  // it("simulates click events", () => {
  //   const onClick = sinon.spy();
  //   const wrapper = mount(<WyswietlDokumentBox />);
  //   wrapper.find("button#odrzucejszyn").simulate("click");
  //   expect((onClick = { handleClickOpen })).to.have.property("open", true);
  // });

  // it("renders table from <ListaMoichDokumentowTable />", () => {
  //   const wrapper = shallow(<ListaMoichDokumentow />);
  //   expect(wrapper.find(ListaMoichDokumentowTable)).to.have.lengthOf(1);
  // });

  //   it("Renders children when passed in", () => {
  //     const wrapper = shallow(
  //       <WyswietlDokumentBox>
  //         <Fragment />
  //       </WyswietlDokumentBox>
  //     );
  //     expect(wrapper.length).toBe(1);
  //   });
});
