import React from "react";
import { createMount } from "@material-ui/core/test-utils";
import HeaderSuperAdmin from "./../Components/Superadmin/Layout/HeaderSuperAdmin";

describe("HeaderSuperAdmin Component", () => {
  let mount;

  beforeEach(() => {
    mount = createMount();
  });

  it("Should render Lista Kategorii button in header", () => {
    const component = mount(<HeaderSuperAdmin />);
    const wrapper = component.find("button").filterWhere(item => {
      return item.prop("data-test") === "listaKategorii";
    });
    expect(wrapper.length).toBe(1);
  });

  it("Should render Dodaj Kategorie button in header", () => {
    const component = mount(<HeaderSuperAdmin />);
    const wrapper = component.find("button").filterWhere(item => {
      return item.prop("data-test") === "dodajKategorie";
    });
    expect(wrapper.length).toBe(1);
  });

  it("Should render Dodaj Uzytkownika button in header", () => {
    const component = mount(<HeaderSuperAdmin />);
    const wrapper = component.find("button").filterWhere(item => {
      return item.prop("data-test") === "dodajkonto";
    });
    expect(wrapper.length).toBe(1);
  });

  it("Should render Edycja Konta button in header", () => {
    const component = mount(<HeaderSuperAdmin />);
    const wrapper = component.find("button").filterWhere(item => {
      return item.prop("data-test") === "edycjaKonta";
    });
    expect(wrapper.length).toBe(1);
  });
});
