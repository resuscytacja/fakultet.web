import React from "react";
import { createMount } from "@material-ui/core/test-utils";
import EdycjaForm from "./../Components/Dokumenty/Edycja/EdycjaForm";

describe("EdycjaForm Component", () => {
  const handleChange = jest.fn();
  const handleEdit = jest.fn();
  const handleFileDelete = jest.fn();

  let mount;
  let edycjaState = {
    pond: {},
    documentTypeList: [],
    documentId: "",
    documentTypeName: "",
    status: "",
    fileList: ["Ddd.pdf", "dddd.pdf"],
    removeFiles: [],
    name: "",
    title: "",
    id: "",
    errors: {},
    errorsBackend: ""
  };

  beforeEach(() => {
    mount = createMount();
  });

  it("Should return two remove file buttons if two files are inside fileList array passed from props", () => {
    const component = mount(
      <EdycjaForm
        {...edycjaState}
        handleChange={handleChange}
        handleEdit={handleEdit}
        handleFileDelete={handleFileDelete}
      />
    );

    const wrapper = component.find("button").filterWhere(item => {
      return item.prop("data-test") === "fileDeleteButton";
    });

    // console.log(component.debug());
    // console.log(wrapper);
    expect(wrapper.length).toBe(2);
  });
});
