import React from "react";
import { shallow } from "enzyme";
import ExampleComponent from "./../ExampleComponent";

// test();
// describe() tylko opisuje testy w srodku

const setUp = (props = {}) => {
  const component = shallow(<ExampleComponent {...props} />);
  return component;
};

describe("Example Component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("It should render without errors", () => {
    // const component = setUp();
    // console.log(component.debug());
    const wrapper = component.find(`[data-test='hehe']`);
    expect(wrapper.length).toBe(1);
  });
});
