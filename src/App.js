import React, { Component } from "react";
import Header from "./Components/Layout/HeaderStudent";
import HeaderModerator from "./Components/Layout/HeaderModerator";
import HeaderAdministrator from "./Components/Layout/HeaderAdministrator";
import LogowanieStudent from "./Components/Logowanie/LogowanieStudent";
import Home from "./Components/Home/Home";
import { BrowserRouter, Route } from "react-router-dom";
import decode from "jwt-decode";
import DodajKonto from "./Components/Superadmin/DodajKonto/DodajKonto";
import WyslijDokument from "./Components/Dokumenty/Wysylanie/WyslijDokument";
import HeaderSuperAdmin from "./Components/Superadmin/Layout/HeaderSuperAdmin";
import LogowanieSuperAdmin from "./Components/Superadmin/Logowanie/LogowanieSuperAdmin";
import WyswietlDokument from "./Components/Dokumenty/WyswietlanieSzczegolowe/WyswietlDokument";
import ListaMoichDokumentow from "./Components/Dokumenty/MojeDokumenty/ListaMoichDokumentow";
import DodajKategorie from "./Components/Superadmin/DodajKategorie/DodajKategorie";
import ListaKategorii from "./Components/Superadmin/ListaKategorii/ListaKategorii";
import EdycjaKonta from "./Components/EdycjaKonta/EdycjaKonta";
import WyslijEmailDoNiezalogowanego from "./Components/Email/WyslijEmailDoNiezalogowanego";
import Edycja from "./Components/Dokumenty/Edycja/Edycja";

class App extends Component {
  constructor(props) {
    super(props);
    this.setSuperAdminHeader = this.setSuperAdminHeader.bind(this);
  }

  state = {
    isLoggedIn: false,
    token: null,
    loggedInUserEmail: null,
    loggedInFirstLastName: "",
    status: "",
    loggedInUserRole: "",
    tokenFakultet: null,
    loginSuperAdmin: false
  };

  componentDidMount() {
    const tokenFromLocalStorage = localStorage.getItem("tokenFakultet");
    if (tokenFromLocalStorage === "" || tokenFromLocalStorage === null) {
      this.setLoggedOutUser();
    } else {
      this.setLoggedInUserData(tokenFromLocalStorage);
    }
  }

  setLoggedInUserData = token => {
    let jwtDecoded = decode(token);
    this.setState({
      isLoggedIn: true,
      token: token,
      loggedInUserRole:
        jwtDecoded[
          "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
        ],
      loggedInUserEmail: jwtDecoded.sub,
      loggedInFirstLastName:
        jwtDecoded.given_name + " " + jwtDecoded.family_name
    });
    localStorage.setItem("tokenFakultet", token);
  };

  setLoggedOutUser = () => {
    this.setState({
      isLoggedIn: false,
      token: undefined,
      loggedInUserEmail: undefined,
      loggedInFirstLastName: null,
      loggedInUserRole: undefined
    });
    localStorage.removeItem("tokenFakultet");
  };

  setSuperAdminHeader = () => {
    this.setState({
      loginSuperAdmin: true
    });
  };

  render() {
    return (
      <BrowserRouter>
        <div>
          {this.state.loggedInUserRole === "Superadmin" && (
            <Route
              data-test="headerSuperAdmin"
              render={props => (
                <HeaderSuperAdmin
                  {...this.state}
                  onLogout={this.setLoggedOutUser}
                  {...props}
                />
              )}
            />
          )}

          {(this.state.loggedInUserRole === "Student" ||
            this.state.loggedInUserRole === undefined) && (
            <Route
              data-test="header"
              render={props => (
                <Header
                  {...this.state}
                  onLogout={this.setLoggedOutUser}
                  {...props}
                />
              )}
            />
          )}
          {this.state.loggedInUserRole === "Moderator" && (
            <Route
              data-test="headerModerator"
              render={props => (
                <HeaderModerator
                  {...this.state}
                  onLogout={this.setLoggedOutUser}
                  {...props}
                />
              )}
            />
          )}
          {this.state.loggedInUserRole === "Admin" && (
            <Route
              data-test="headerAdministrator"
              render={props => (
                <HeaderAdministrator
                  {...this.state}
                  onLogout={this.setLoggedOutUser}
                  {...props}
                />
              )}
            />
          )}
          <Route path="/" exact component={Home} />
          <Route path="/Home" exact component={Home} />
          <Route
            path="/superadmin/logowanie"
            render={props => (
              <LogowanieSuperAdmin
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
                setSuperAdminHeader={this.setSuperAdminHeader}
              />
            )}
          />
          <Route
            path="/superadmin/dodajKategorie"
            render={props => (
              <DodajKategorie
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/superadmin/listaKategorii"
            render={props => (
              <ListaKategorii
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/superadmin/edycjaKonta"
            render={props => (
              <EdycjaKonta
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/logowanie"
            render={props => (
              <LogowanieStudent
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/superadmin/dodajkonto"
            render={props => (
              <DodajKonto
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          {/* <Route
            path={"/dokumenty/wyswietl/:advId"}
            render={props => <WyswietlDokument {...props} {...this.state} />}
          /> */}
          <Route
            path={"/dokumenty/wyswietl/:docId"}
            render={props => <WyswietlDokument {...props} {...this.state} />}
          />
          <Route
            path="/wyslijDokument"
            render={props => (
              <WyslijDokument
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/mojeDokumenty"
            render={props => (
              <ListaMoichDokumentow
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/edycjaKonta"
            render={props => (
              <EdycjaKonta
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path="/wyslijEmail"
            render={props => (
              <WyslijEmailDoNiezalogowanego
                {...props}
                {...this.state}
                onLogin={this.setLoggedInUserData}
              />
            )}
          />
          <Route
            path={"/dokumenty/edytuj/:docId"}
            render={props => <Edycja {...props} {...this.state} />}
          />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
