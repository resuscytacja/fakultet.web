export function handle400Errors(errorsKeyValueList) {
  let errorResult = "";
  for (var key in errorsKeyValueList) {
    if (errorsKeyValueList.hasOwnProperty(key)) {
      console.log(key + " -> " + errorsKeyValueList[key]);
      errorResult = errorResult.concat(errorsKeyValueList[key]);
    }
    errorResult += "\n";
  }

  return errorResult;
}
export function convertToCorrectDate(date) {
  var currentDateFormated =
    date.getFullYear() +
    "-" +
    addZero(date.getMonth() + 1) +
    "-" +
    addZero(date.getDate());

  return currentDateFormated;
}
function addZero(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

export function capitalizeWord(word) {
  if (typeof word !== "string") return "";
  return word.charAt(0).toUpperCase() + word.slice(1);
}
