import React, { Fragment, Component } from "react";
import { withStyles } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import PowerSettingsNew from "@material-ui/icons/PowerSettingsNew";
import List from "@material-ui/icons/List";
import Settings from "@material-ui/icons/Settings";
import VerifyingAcceptingNotifications from "./VerifyingAcceptingNotifications";

class HeaderAdministrator extends Component {
  _isMounted = false;

  state = {
    anchorEl: null,
    notifications: []
  };

  componentDidMount() {
    this._isMounted = true;

    if (this.props.isLoggedIn) {
      this.displayNotifications(this.props.token);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.displayNotifications(nextProps.token);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  displayNotifications(token) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    };

    let apiUrl =
      "https://localhost:44345/api/Document/documentList?orderBy=SendTime&orderType=desc";

    fetch(apiUrl, fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({
            notifications: data.documents.documents.map(document => ({
              documentId: document.documentId,
              content: "Dokument '" + document.title + "'",
              secondaryContent: "Kategoria: " + document.documentTypeName
            }))
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="inherit">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Fakultet - panel admina
            </Typography>
            <div className={classes.section}>
              {this.props.isLoggedIn && (
                <div className={classes.grow}>
                  <Link
                    style={{ textDecoration: "none", color: "white" }}
                    to="/mojeDokumenty"
                  >
                    <Button
                      varinat="contained"
                      color="inherit"
                      className={classes.button}
                    >
                      <List />
                      Lista dokumentów
                    </Button>
                  </Link>
                  <Link
                    style={{ textDecoration: "none", color: "white" }}
                    to="/edycjaKonta"
                  >
                    <Button
                      varinat="contained"
                      color="inherit"
                      className={classes.button}
                    >
                      <Settings />
                      Edycja konta
                    </Button>
                  </Link>
                  <VerifyingAcceptingNotifications
                    notifications={this.state.notifications}
                    history={this.props.history}
                    mode={"accepting"}
                  />
                  <Button
                    varinat="contained"
                    color="inherit"
                    className={classes.button}
                    onClick={this.props.onLogout}
                  >
                    <PowerSettingsNew />
                  </Button>
                </div>
              )}
              <div className={classes.grow}>
                {this.props.isLoggedIn === false && (
                  <Fragment>
                    <Link
                      style={{ textDecoration: "none", color: "white" }}
                      to="/logowanie"
                    >
                      <Button
                        varinat="contained"
                        color="inherit"
                        className={classes.button}
                      >
                        Zaloguj się
                      </Button>
                    </Link>
                  </Fragment>
                )}
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1,
    color: "black"
  },
  grow: {
    flexGrow: 1,
    color: "black"
  },
  section: {
    justifyContent: "flex-end"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  button: {
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 5,
    padding: 10,
    color: "black"
  }
};
export default withStyles(styles)(HeaderAdministrator);
