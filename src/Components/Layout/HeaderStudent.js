import React, { Fragment, Component } from "react";
import { withStyles } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import PowerSettingsNew from "@material-ui/icons/PowerSettingsNew";
import Add from "@material-ui/icons/Add";
import List from "@material-ui/icons/List";
import Settings from "@material-ui/icons/Settings";
import StudentNotifications from "./StudentNotifications";
import Email from "@material-ui/icons/Email";
import AssignmentInd from "@material-ui/icons/AssignmentInd";

class HeaderStudent extends Component {
  state = {
    anchorEl: null,
    notifications: []
  };

  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.displayNotifications(this.props.token);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.displayNotifications(nextProps.token);
    }
  }

  displayNotifications(token) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    };

    fetch("https://localhost:44345/api/Notification/my", fetchData)
      .then(resp => resp.json())
      .then(data => {
        this.setState({
          notifications: data
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  markNotificationRead = notificationId => {
    let selectedNotificationIndex = this.state.notifications.findIndex(
      notification => notification.notificationId === notificationId
    );

    let updatedNotificationsList = this.state.notifications.map(
      (notification, index) => {
        if (index !== selectedNotificationIndex) return notification;
        return { ...notification, read: true };
      }
    );

    let fetchData = {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch(
      "https://localhost:44345/api/notification/markasread/" + notificationId,
      fetchData
    )
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        this.setState({ notifications: updatedNotificationsList });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  // Autor: Tomasz Zysk
  markAllNotificationsRead = () => {
    let updatedNotificationsList = this.state.notifications.map(
      notification => {
        if (notification.read) return notification;
        return { ...notification, read: true };
      }
    );

    let fetchData = {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch("https://localhost:44345/api/notification/markasread/all", fetchData)
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        this.setState({ notifications: updatedNotificationsList });
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Fakultet - panel użytkownika
            </Typography>
            <div className={classes.section}>
              {this.props.isLoggedIn && (
                <div className={classes.grow}>
                  <Link
                    style={{ textDecoration: "none", color: "white" }}
                    to="/wyslijEmail"
                  >
                    <Button
                      varinat="contained"
                      color="inherit"
                      className={classes.button}
                    >
                      <Email />
                      Wyślij wiadomość
                    </Button>
                  </Link>
                  <Link
                    style={{ textDecoration: "none", color: "white" }}
                    to="/mojeDokumenty"
                  >
                    <Button
                      varinat="contained"
                      color="inherit"
                      className={classes.button}
                    >
                      <List />
                      Moje dokumenty
                    </Button>
                  </Link>
                  <Link
                    style={{ textDecoration: "none", color: "white" }}
                    to="/wyslijDokument"
                  >
                    <Button
                      varinat="contained"
                      color="inherit"
                      className={classes.button}
                    >
                      <Add />
                      Wyślij dokument
                    </Button>
                  </Link>
                  <Link
                    style={{ textDecoration: "none", color: "white" }}
                    to="/edycjaKonta"
                  >
                    <Button
                      varinat="contained"
                      color="inherit"
                      className={classes.button}
                    >
                      <Settings />
                      Edycja konta
                    </Button>
                  </Link>
                  <StudentNotifications
                    notifications={this.state.notifications}
                    markNotificationRead={this.markNotificationRead}
                    markAllNotificationsRead={this.markAllNotificationsRead}
                    history={this.props.history}
                  />
                  <Button
                    varinat="contained"
                    color="inherit"
                    className={classes.button}
                    onClick={this.props.onLogout}
                  >
                    <PowerSettingsNew />
                  </Button>
                </div>
              )}
              <div className={classes.grow}>
                {this.props.isLoggedIn === false && (
                  <Fragment>
                    <Link
                      style={{ textDecoration: "none", color: "white" }}
                      to="/logowanie"
                    >
                      <Button
                        varinat="contained"
                        color="inherit"
                        className={classes.button}
                      >
                        <AssignmentInd />
                        Zaloguj się
                      </Button>
                    </Link>
                  </Fragment>
                )}
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  section: {
    justifyContent: "flex-end"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  button: {
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 5,
    padding: 10
  }
};
export default withStyles(styles)(HeaderStudent);
