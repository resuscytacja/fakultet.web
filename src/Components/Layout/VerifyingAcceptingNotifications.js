import React, { Fragment, Component } from "react";
import { withStyles } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import Avatar from "@material-ui/core/Avatar";
import Badge from "@material-ui/core/Badge";
import ListItemText from "@material-ui/core/ListItemText";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";

class VerifyingAcceptingNotifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorElNotifications: null
    };
  }

  handleVerifyingAcceptingNotificationsOpen = event => {
    this.setState({ anchorElNotifications: event.currentTarget });
  };

  handleVerifyingAcceptingNotificationsClose = () => {
    this.setState({ anchorElNotifications: null });
  };

  redirectAndCloseMenu = documentId => {
    this.handleVerifyingAcceptingNotificationsClose();
    this.props.history.push("/dokumenty/wyswietl/" + documentId);
  };

  render() {
    const { classes } = this.props;
    const { anchorElNotifications } = this.state;
    const isVerifyingAcceptingNotificationsOpen = Boolean(
      anchorElNotifications
    );
    let notificationsCount = this.props.notifications.length;

    return (
      <Fragment>
        <Button
          aria-owns={
            isVerifyingAcceptingNotificationsOpen
              ? "material-appbar-notifications"
              : undefined
          }
          aria-haspopup="true"
          onClick={this.handleVerifyingAcceptingNotificationsOpen}
          color="inherit"
        >
          {notificationsCount > 0 && (
            <Badge badgeContent={notificationsCount} color="secondary">
              <NotificationsIcon />
            </Badge>
          )}
          {notificationsCount === 0 && <NotificationsIcon />}
        </Button>
        <Menu
          anchorEl={anchorElNotifications}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          open={isVerifyingAcceptingNotificationsOpen}
          onClose={this.handleVerifyingAcceptingNotificationsClose}
          PaperProps={{
            style: {
              maxHeight: 80 * 4.5,
              width: 500
            }
          }}
        >
          {this.props.notifications.length === 0 && (
            <ListItem key="0" button>
              <ListItemText
                primary={
                  this.props.mode === "verifying"
                    ? "BRAK DOKUMENTOW DO WERYFIKACJI"
                    : "BRAK DOKUMENTOW DO AKCEPTACJI"
                }
                primaryTypographyProps={{
                  variant: "button",
                  color: "primary",
                  align: "right"
                }}
              />
            </ListItem>
          )}
          {notificationsCount > 0 && (
            <ListItem key="0" button>
              <ListItemText
                primary={
                  this.props.mode === "verifying"
                    ? `Masz ${notificationsCount} dokumentów do weryfikacji`
                    : `Masz ${notificationsCount} dokumentów do akceptacji`
                }
                primaryTypographyProps={{
                  variant: "button",
                  color: "primary",
                  align: "right"
                }}
              />
            </ListItem>
          )}
          {this.props.notifications.map(notification => (
            <ListItem
              key={notification.documentId}
              button
              onClick={() => this.redirectAndCloseMenu(notification.documentId)}
              classes={{
                root: classes.unreadNotification // class name, e.g. `classes-nesting-root-x`
              }}
            >
              <ListItemAvatar>
                <Avatar>
                  <InsertDriveFileIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={notification.content}
                secondary={notification.secondaryContent}
              />
              <ListItemSecondaryAction className={classes.listItemSecondary}>
                <IconButton
                  aria-label="GoTo"
                  onClick={() =>
                    this.redirectAndCloseMenu(notification.documentId)
                  }
                >
                  <ArrowForwardIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </Menu>
      </Fragment>
    );
  }
}

const styles = {
  listItemSecondary: {
    right: 14
  },
  unreadNotification: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)"
  },
  grow: {
    flexGrow: 1
  }
};
export default withStyles(styles)(VerifyingAcceptingNotifications);
