import React, { Fragment, Component } from "react";
import { withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ArchiveIcon from "@material-ui/icons/Archive";
import AssignmentReturnedIcon from "@material-ui/icons/AssignmentReturned";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import Avatar from "@material-ui/core/Avatar";
import Badge from "@material-ui/core/Badge";
import ListItemText from "@material-ui/core/ListItemText";
import NotificationsIcon from "@material-ui/icons/Notifications";
import WarningIcon from "@material-ui/icons/Warning";
import DoneOutlineIcon from "@material-ui/icons/DoneOutline";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";

class StudentNotifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorElNotifications: null
    };
  }

  handleStudentNotificationsOpen = event => {
    this.setState({ anchorElNotifications: event.currentTarget });
  };

  handleStudentNotificationsClose = () => {
    this.setState({ anchorElNotifications: null });
  };

  redirectAndCloseMenu = (link, read, notificationId) => {
    this.handleStudentNotificationsClose();
    this.props.history.push(link);
    if (!read) {
      this.props.markNotificationRead(notificationId);
    }
  };

  render() {
    const { classes } = this.props;
    const { anchorElNotifications } = this.state;
    const isStudentNotificationsOpen = Boolean(anchorElNotifications);
    let unreadNotificationsCount = this.props.notifications.filter(
      notification => !notification.read
    ).length;

    return (
      <Fragment>
        <Button
          aria-owns={
            isStudentNotificationsOpen
              ? "material-appbar-notifications"
              : undefined
          }
          aria-haspopup="true"
          onClick={this.handleStudentNotificationsOpen}
          color="inherit"
        >
          {unreadNotificationsCount > 0 && (
            <Badge badgeContent={unreadNotificationsCount} color="secondary">
              <NotificationsIcon />
            </Badge>
          )}
          {unreadNotificationsCount === 0 && <NotificationsIcon />}
        </Button>
        <Menu
          anchorEl={anchorElNotifications}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          open={isStudentNotificationsOpen}
          onClose={this.handleStudentNotificationsClose}
          PaperProps={{
            style: {
              maxHeight: 80 * 4.5,
              width: 500
            }
          }}
        >
          {this.props.notifications.length === 0 && (
            <ListItem key="0" button>
              <ListItemText
                primary={"BRAK POWIADOMIEN"}
                primaryTypographyProps={{
                  variant: "button",
                  color: "primary",
                  align: "right"
                }}
              />
            </ListItem>
          )}
          {unreadNotificationsCount > 0 && (
            <ListItem
              key="0"
              classes={{
                root: classes.readNotification
              }}
            >
              <ListItemSecondaryAction className={classes.listItemSecondary}>
                <IconButton
                  aria-label="Delete"
                  onClick={() => this.props.markAllNotificationsRead()}
                >
                  <Typography
                    variant="button"
                    color="inherit"
                    className={classes.grow}
                  >
                    Odczytaj wszystkie
                  </Typography>
                  <ArchiveIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          )}
          {this.props.notifications.map(notification => (
            <ListItem
              key={notification.notificationId}
              button
              onClick={() =>
                this.redirectAndCloseMenu(
                  `/dokumenty/wyswietl/${notification.documentId}`,
                  notification.read,
                  notification.notificationId
                )
              }
              classes={{
                root: notification.read
                  ? classes.readNotification
                  : classes.unreadNotification // class name, e.g. `classes-nesting-root-x`
              }}
            >
              <ListItemAvatar>
                <Avatar>
                  {notification.type === "ReturnedDocument" && (
                    <AssignmentReturnedIcon />
                  )}
                  {notification.type === "AcceptedDocument" && (
                    <AssignmentTurnedInIcon />
                  )}
                  {notification.type === "CancelledDocument" && (
                    <HighlightOffIcon />
                  )}
                  {notification.type === "UnverifiedDocument" && (
                    <WarningIcon />
                  )}
                  {notification.type === "VerifiedDocument" && (
                    <DoneOutlineIcon />
                  )}
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={notification.content}
                secondary={notification.secondaryContent}
              />
              <ListItemSecondaryAction className={classes.listItemSecondary}>
                <IconButton
                  aria-label="MarkAsRead"
                  onClick={() =>
                    this.props.markNotificationRead(notification.notificationId)
                  }
                >
                  {!notification.read && <ArchiveIcon />}
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </Menu>
      </Fragment>
    );
  }
}

const styles = {
  listItemSecondary: {
    right: 14
  },
  unreadNotification: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)"
  },
  grow: {
    flexGrow: 1
  }
};
export default withStyles(styles)(StudentNotifications);
