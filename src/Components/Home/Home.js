import React, { Component } from "react";
import { withStyles } from "@material-ui/core/";

class Home extends Component {
  render() {
    const { classes } = this.props;
    return (
      <h1 className={classes.header}>
        Witaj w serwisie obiegu dokumentów! Zaloguj się, aby skorzystać z
        naszego serwisu.
      </h1>
    );
  }
}
const styles = theme => ({
  header: {
    width: "auto",
    // display: "block",
    // marginLeft: theme.spacing(3),
    // marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
      width: "80%",
      marginLeft: "auto",
      marginRight: "auto"
    }
  }
});

export default withStyles(styles)(Home);
