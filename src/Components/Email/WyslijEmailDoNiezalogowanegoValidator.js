import { Field, Schema } from "v4f";
export default Schema(
  {
    title: Field()
      .string()
      .max(50, { message: "Tytuł nie może przekraczać 50 znaków" })
      .min(3, { message: "Tytuł musi być dłuższy niż 3 znaki" })
      .required({ message: "Tytuł jest wymagany" }),
    email: Field()
      .string()
      .email({ message: "Podano nieprawidlowy adres email" })
      .required({ message: "Email jest wymagany" }),
    content: Field()
      .string()
      .max(150, { message: "Wiadomość nie może przekraczać 50 znaków" })
      .min(3, { message: "Wiadomość musi być dłuższa niż 3 znaki" })
      .required({ message: "Treść wiadomości jest wymagana" })
  },
  { verbose: true, async: true }
);
