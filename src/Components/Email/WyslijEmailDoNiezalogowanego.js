import React, { Component } from "react";
import { handle400Errors } from "../../../src/Helpers/helpers";
import WyslijEmailDoNiezalogowanegoForm from "./WyslijEmailDoNiezalogowanegoForm";
import WyslijEmailDoNiezalogowanegoValidator from "./WyslijEmailDoNiezalogowanegoValidator";
class WyslijEmailDoNiezalogowanego extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    pond: {},
    documentTypeList: [],
    documentId: "",
    fileList: [],
    name: "",
    id: "",
    email: "",
    content: "",
    errors: {},
    errorsBackend: ""
  };

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
  }

  handleSubmit = pond => event => {
    event.preventDefault();
    WyslijEmailDoNiezalogowanegoValidator.validate(this.state)
      .then(() => {
        this.submitDocument(pond);
      })
      .catch(errors => {
        this.setState({ errors });
      });
  };

  submitDocument(pond) {
    let formData = new FormData();
    formData.append("Title", this.state.title);
    formData.append("EmailRecipient", this.state.email);
    formData.append("Content", this.state.content);

    pond
      .getFiles()
      .map(fileItem => fileItem.file)
      .forEach(file => {
        formData.append("FileList", file, file.name);
      });

    let fetchData = {
      body: formData,
      method: "POST",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch(
      "https://localhost:44345/api/Email/send_email_to_unregistered",
      fetchData
    )
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        this.props.history.push("/mojeDokumenty");
      })
      .catch(error => {
        error.then(errorsKeyValueList => {
          this.setState({ errorsBackend: handle400Errors(errorsKeyValueList) });
        });
      });
  }

  testRef = ref => {
    this.setState({ pond: ref });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDirty(e) {
    const { name, value } = e.target;

    const isValid = WyslijEmailDoNiezalogowanegoValidator[name].validate(
      value,
      {
        verbose: true,
        values: this.props.state
      }
    );

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  }

  render() {
    return (
      <WyslijEmailDoNiezalogowanegoForm
        {...this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        handleDirty={this.handleDirty}
        onDrop={this.onDrop}
        testRef={this.testRef}
      />
    );
  }
}

export default WyslijEmailDoNiezalogowanego;
