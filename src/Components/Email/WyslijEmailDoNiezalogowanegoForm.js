import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  Typography,
  withStyles,
  TextField
} from "@material-ui/core/";
import Drafts from "@material-ui/icons/Drafts";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import { Link } from "react-router-dom";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
registerPlugin(FilePondPluginFileValidateType);

class WyslijEmailDoNiezalogowanegoForm extends Component {
  pond = {};

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <Drafts />
          </Avatar>
          <br />
          <Typography component="h1" variant="h5">
            Wyślij wiadomość
          </Typography>
          <br />
          <form className={classes.form} encType="multipart/form-data">
            <br />
            <TextField
              label="Tytuł"
              id="title"
              name="title"
              className={classes.title}
              onBlur={this.props.handleDirty}
              value={this.props.title}
              error={this.props.errors.title !== undefined}
              helperText={this.props.errors.title}
              onChange={this.props.handleChange("title")}
              fullWidth
              margin="normal"
              multiline
              rows="1"
              variant="outlined"
              required
            />
            <TextField
              label="Email odbiorcy"
              id="email"
              name="email"
              onBlur={this.props.handleDirty}
              value={this.props.email}
              error={this.props.errors.email !== undefined}
              helperText={this.props.errors.email}
              onChange={this.props.handleChange("email")}
              fullWidth
              margin="normal"
              multiline
              rows="1"
              variant="outlined"
              required
            />
            <TextField
              label="Wyślij wiadomość..."
              id="content"
              name="content"
              className={classes.title}
              onBlur={this.props.handleDirty}
              value={this.props.content}
              error={this.props.errors.content !== undefined}
              helperText={this.props.errors.content}
              onChange={this.props.handleChange("content")}
              fullWidth
              margin="normal"
              multiline
              rows="4"
              variant="outlined"
              required
            />
            <FilePond
              ref={ref => (this.pond = ref)}
              allowMultiple={true}
              acceptedFileTypes={[
                "application/pdf",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/msword",
                "application/vnd.oasis.opendocument.text"
              ]}
              labelIdle="Kliknij, aby dodać pliki lub upuść w ten obszar"
            />
            <Typography className={classes.formats}>
              Obsługiwane formaty to: *pdf *doc *docx *odt
            </Typography>

            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.props.handleSubmit(this.pond)}
            >
              Wyślij wiadomość
            </Button>
            <Link className={classes.link} to="/mojeDokumenty">
              <Button
                variant="contained"
                color="secondary"
                className={classes.submit}
              >
                Anuluj
              </Button>
            </Link>
          </form>
        </Paper>
      </main>
    );
  }
}

const styles = theme => ({
  main: {
    width: "auto",
    display: "block",
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
      height: 700
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 100
  },
  avatar: {
    marginTop: 10,
    backgroundColor: theme.palette.primary.main
  },
  category: {
    width: 450,
    display: "flex",
    justifyContent: "space-between"
  },
  formats: {
    textDecoration: "none",
    color: "grey",
    fontSize: "14px",
    alignItems: "center"
  },
  link: {
    textDecoration: "none"
  },
  submit: {
    marginBottom: 20,
    marginLeft: 70,
    width: 190
  }
});

export default withStyles(styles)(WyslijEmailDoNiezalogowanegoForm);
