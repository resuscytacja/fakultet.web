import { Field, Schema } from "v4f";

export default Schema(
  {
    oldPassword: Field()
      .string()
      .min(6, { message: "Haslo musi skladac sie z przynajmniej 6 znakow" })
      .required({ message: "Haslo jest wymagane" }),
    newPassword: Field()
      .string()
      .min(6, { message: "Haslo musi skladac sie z przynajmniej 6 znakow" })
      .required({ message: "Haslo jest wymagane" }),
    confirmNewPassword: Field()
      .string()
      .min(6, { message: "Haslo musi skladac sie z przynajmniej 6 znakow" })
      .equals(["#newPassword"], { message: "Podane hasla sie nie zgadzaja" })
      .required({ message: "Powtorzenie hasla jest wymagane" })
  },
  { verbose: true, async: true }
);
