import React, { Component } from "react";
import UstawieniaHasloValidator from "./UstawieniaHasloValidator";
import EdycjaKontaFrom from "./EdycjaKontaFrom";
import decode from "jwt-decode";
import { handle400Errors } from "../../Helpers/helpers";

class EdycjaKonta extends Component {
  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    open: false,
    snackbarMessage: "",
    expanded: null,
    password: "",
    oldPassword: "",
    newPassword: "",
    confirmNewPassword: "",
    errors: {},
    errorsBackend: "",
    isLoading: false
  };

  componentDidMount() {
    if (this.props.token !== null && this.props.token !== undefined) {
      let jwtDecoded = decode(this.props.token);
      this.setState({
        firstName: jwtDecoded.family_name,
        lastName: jwtDecoded.given_name,
        email: jwtDecoded.sub
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
  }

  handleOpenSnackbar = snackbarMessage => {
    this.setState({ snackbarMessage, open: true });
  };

  handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  handleExpand = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false
    });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDirty = coZmieniono => event => {
    const { name, value } = event.target;

    let isValid = false;
    if (coZmieniono === "password") {
      isValid = UstawieniaHasloValidator[name].validate(value, {
        verbose: true,
        values: this.state
      });
    }

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  };

  handleSubmit = coZmieniono => event => {
    this.setState({ isLoading: true });

    event.preventDefault();
    if (coZmieniono === "password") {
      UstawieniaHasloValidator.validate(this.state)
        .then(data => {
          this.handleUpdatePassword();
        })
        .catch(errors => {
          this.setState({ errors, isLoading: false });
        });
    }
  };

  handleUpdatePassword() {
    let personalDataToUpdate = {
      oldPassword: this.state.oldPassword,
      newPassword: this.state.newPassword,
      confirmNewPassword: this.state.confirmNewPassword
    };

    let fetchData = {
      method: "PUT",
      body: JSON.stringify(personalDataToUpdate),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch("https://localhost:44345/api/Account/editPassword", fetchData)
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response.json();
      })
      .then(data => {
        this.setState({
          oldPassword: "",
          newPassword: "",
          confirmNewPassword: "",
          isLoading: false,
          errorsBackend: ""
        });
        this.handleOpenSnackbar("Zaktualizowano haslo");
      })
      .catch(error => {
        error.then(errorsKeyValueList => {
          this.setState({
            errorsBackend: handle400Errors(errorsKeyValueList),
            isLoading: false
          });
        });
      });
  }
  render() {
    return (
      <EdycjaKontaFrom
        {...this.state}
        {...this.props}
        handleDirty={this.handleDirty}
        handleExpand={this.handleExpand}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        handleOpenSnackbar={this.handleOpenSnackbar}
        handleCloseSnackbar={this.handleCloseSnackbar}
      />
    );
  }
}

export default EdycjaKonta;
