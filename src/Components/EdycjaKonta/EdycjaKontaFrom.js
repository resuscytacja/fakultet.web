import React, { Component } from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { TextField, Typography, withStyles, Button } from "@material-ui/core/";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import CircularProgress from "@material-ui/core/CircularProgress";
import Paper from "@material-ui/core/Paper";

class EdycjaKontaForm extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.main}>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={this.props.open}
          autoHideDuration={3000}
          onClose={this.props.handleCloseSnackbar}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{this.props.snackbarMessage}</span>}
          action={[
            <Button
              key="undo"
              color="inherit"
              size="small"
              onClick={this.props.handleCloseSnackbar}
            >
              Ok
            </Button>,
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.props.handleCloseSnackbar}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        <ExpansionPanel
          width="50%"
          expanded={this.props.expanded === "panelZmienHaslo"}
          onChange={this.props.handleExpand("panelZmienHaslo")}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>
              Zaktualizuj hasło
            </Typography>
            <Typography className={classes.secondaryHeading}>
              Zmień haslo
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <form className={classes.name}>
              <TextField
                label="Stare haslo"
                id="oldPassword"
                name="oldPassword"
                type="password"
                value={this.props.oldPassword}
                onChange={this.props.handleChange("oldPassword")}
                onBlur={this.props.handleDirty("password")}
                error={this.props.errors.oldPassword !== undefined}
                helperText={this.props.errors.oldPassword}
                fullWidth
                margin="normal"
                required
              />
              <br />
              <TextField
                label="Nowe haslo"
                id="newPassword"
                name="newPassword"
                type="password"
                value={this.props.newPassword}
                onChange={this.props.handleChange("newPassword")}
                onBlur={this.props.handleDirty("password")}
                error={this.props.errors.newPassword !== undefined}
                helperText={this.props.errors.newPassword}
                fullWidth
                margin="normal"
                required
              />
              <br />
              <TextField
                label="Nowe hasło ponownie"
                id="confirmNewPassword"
                name="confirmNewPassword"
                type="password"
                value={this.props.confirmNewPassword}
                onChange={this.props.handleChange("confirmNewPassword")}
                onBlur={this.props.handleDirty("password")}
                error={this.props.errors.confirmNewPassword !== undefined}
                helperText={this.props.errors.confirmNewPassword}
                fullWidth
                margin="normal"
                required
              />
              <br />
              <br />
              <p style={{ color: "red", whiteSpace: "pre-wrap" }}>
                {this.props.errorsBackendPassword}
              </p>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.props.handleSubmit("password")}
                disabled={this.props.isLoading}
              >
                Zapisz
              </Button>
              {this.props.isLoading && (
                <CircularProgress
                  size={24}
                  className={classes.buttonProgress}
                />
              )}
              <p style={{ color: "red", whiteSpace: "pre-wrap" }}>
                {this.props.errorsBackend}
              </p>
            </form>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Paper>
    );
  }
}

const styles = theme => ({
  main: {
    width: "70%",
    //alignItems: "center"
    padding: "0.5%",
    margin: "auto",
    marginTop: "1%"
  },
  buttonProgress: {
    marginLeft: -50
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "50%",
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  }
});

export default withStyles(styles)(EdycjaKontaForm);
