import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  TextField,
  Typography,
  withStyles
} from "@material-ui/core/";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import CircularProgress from "@material-ui/core/CircularProgress";

class LogowanieStudentForm extends Component {
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        {this.props.isLoggedIn === false && (
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            {this.props.location.state !== undefined && (
              <p style={{ color: "green", whiteSpace: "pre-wrap" }}>
                {this.props.location.state.successfullyRegisteredMessage}
              </p>
            )}

            <Typography component="h1" variant="h5">
              Zaloguj się
            </Typography>
            <form className={classes.form}>
              <TextField
                id="email"
                label="Email"
                name="email"
                value={this.props.email}
                onChange={this.props.handleChange("email")}
                onBlur={this.props.handleDirty}
                error={this.props.errors.email !== undefined}
                helperText={this.props.errors.email}
                fullWidth
                margin="normal"
                required
              />
              <TextField
                label="Haslo"
                id="password"
                name="password"
                type="password"
                value={this.props.password}
                onChange={this.props.handleChange("password")}
                onBlur={this.props.handleDirty}
                error={this.props.errors.password !== undefined}
                helperText={this.props.errors.password}
                fullWidth
                margin="normal"
                required
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.props.handleSubmit}
                disabled={this.props.isLoading}
              >
                Zaloguj
              </Button>
              {this.props.isLoading && (
                <CircularProgress
                  size={24}
                  className={classes.buttonProgress}
                />
              )}
              <p style={{ color: "red", whiteSpace: "pre-wrap" }}>
                {this.props.errorsBackend}
              </p>
            </form>
          </Paper>
        )}
      </main>
    );
  }
}

const styles = theme => ({
  buttonProgress: {
    color: "green",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -24,
    marginLeft: -12
  },
  main: {
    width: "auto",
    display: "block",
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(6))]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto",
      height: 700
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(
      3
    )}px`,
    marginBottom: 100
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1)
  },
  submit: {
    marginTop: theme.spacing(3)
  },
  haslo: {
    fontSize: 14,
    color: "black"
  }
});

export default withStyles(styles)(LogowanieStudentForm);
