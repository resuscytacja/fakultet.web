import { Field, Schema } from "v4f";

export default Schema(
  {
    email: Field()
      .email()
      .required({ message: "Email jest wymagany" }),
    password: Field()
      .string()
      .min(6, { message: "Haslo musi skladac sie z przynajmniej 6 znakow" })
      .required({ message: "Haslo jest wymagane" })
  },
  { verbose: true, async: true }
);
