import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  Typography,
  withStyles,
  TextField,
  FormControl,
  Select,
  MenuItem,
  Snackbar,
  CircularProgress
} from "@material-ui/core/";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

class DodajKategorieForm extends Component {
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Dodaj nową kategorię
          </Typography>
          <form className={classes.form}>
            <TextField
              id="name"
              label="Nazwa kategorii"
              name="name"
              value={this.props.name}
              onChange={this.props.handleChange("name")}
              onBlur={this.props.handleDirty}
              error={this.props.errors.name !== undefined}
              helperText={this.props.errors.name}
              fullWidth
              margin="normal"
              required
            />
            <br />
            <br />
            <FormControl className={classes.formControl} required>
              <Select
                value={this.props.allowedToSendUserAccount}
                onChange={this.props.handleChange("allowedToSendUserAccount")}
                className={classes.selecto}
              >
                <MenuItem value="student">Student</MenuItem>
                <MenuItem value="rektor">Rektor</MenuItem>
                <MenuItem value="dziekan">Dziekan</MenuItem>
                <MenuItem value="pracownikdziekanatu">
                  Pracownik Dziekanatu
                </MenuItem>
              </Select>
            </FormControl>
            <Typography className={classes.underline}>
              Osoba wysyłająca dokument
            </Typography>
            <br />
            <br />
            <FormControl className={classes.formControl} required>
              <Select
                value={this.props.verifyingUserAccount}
                onChange={this.props.handleChange("verifyingUserAccount")}
                className={classes.selecto}
              >
                <MenuItem value="pracownikdziekanatu">
                  Pracownik Dziekanatu
                </MenuItem>
                <MenuItem value="rektor">Rektor</MenuItem>
                <MenuItem value="dziekan">Dziekan</MenuItem>
              </Select>
            </FormControl>
            <Typography className={classes.underline}>
              Osoba weryfikująca poprawność dokumentu{" "}
            </Typography>
            <br />
            <br />
            <FormControl className={classes.formControl} required>
              <Select
                value={this.props.acceptingUserAccount}
                onChange={this.props.handleChange("acceptingUserAccount")}
                className={classes.selecto}
              >
                <MenuItem value="pracownikdziekanatu">
                  Pracownik Dziekanatu
                </MenuItem>
                <MenuItem value="rektor">Rektor</MenuItem>
                <MenuItem value="dziekan">Dziekan</MenuItem>
              </Select>
            </FormControl>
            <Typography className={classes.underline}>
              Osoba rozpatrująca dokument{" "}
            </Typography>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.props.handleSubmit}
              disabled={this.props.isLoading}
            >
              Dodaj
            </Button>
            {this.props.isLoading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
            <p style={{ color: "red", whiteSpace: "pre-wrap" }}>
              {this.props.errorsBackend}
            </p>
          </form>
          <Snackbar
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            open={this.props.open}
            autoHideDuration={3000}
            onClose={this.props.handleCloseSnackbar}
            ContentProps={{
              "aria-describedby": "message-id"
            }}
            message={<span id="message-id">{this.props.snackbarMessage}</span>}
            action={[
              <Button
                key="undo"
                color="inherit"
                size="small"
                onClick={this.props.handleCloseSnackbar}
              >
                Dodano kategorię
              </Button>,
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={this.props.handleCloseSnackbar}
              >
                <CloseIcon />
              </IconButton>
            ]}
          />
        </Paper>
      </main>
    );
  }
}

const styles = theme => ({
  buttonProgress: {
    color: "green",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -24,
    marginLeft: -12
  },
  main: {
    width: "auto",
    display: "block",
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(6))]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  selecto: {
    width: "100%"
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1)
  },
  submit: {
    marginTop: theme.spacing(3)
  },
  underline: {
    textDecoration: "none",
    color: "grey",
    fontSize: "14px",
    alignItems: "center"
  }
});

export default withStyles(styles)(DodajKategorieForm);
