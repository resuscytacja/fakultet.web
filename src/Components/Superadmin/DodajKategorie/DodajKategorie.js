import React, { Component } from "react";
import DodajKategorieValidator from "./DodajKategorieValidator";
import { handle400Errors } from "../../../Helpers/helpers";
import DodajKategorieForm from "./DodajKategorieForm";

class DodajKategorie extends Component {
  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    name: "",
    allowedToSendUserAccount: "student",
    acceptingUserAccount: "pracownikdziekanatu",
    verifyingUserAccount: "dziekan",
    errors: {},
    errorsBackend: "",
    open: false,
    snackbarMessage: "",
    isLoading: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.loggedInUserRole !== "superadmin") {
      this.props.history.push("/Home");
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDirty(e) {
    const { name, value } = e.target;

    const isValid = DodajKategorieValidator[name].validate(value, {
      verbose: true,
      values: this.state
    });

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.addDocumentType();
  }

  addDocumentType() {
    this.setState({ isLoading: true, errorsBackend: "" });

    let documentTypeToCreate = {
      name: this.state.name,
      allowedToSendUserAccount: this.state.allowedToSendUserAccount,
      verifyingUserAccount: this.state.verifyingUserAccount,
      acceptingUserAccount: this.state.acceptingUserAccount
    };

    let fetchData = {
      method: "POST",
      body: JSON.stringify(documentTypeToCreate),
      headers: {
        Authorization: "Bearer " + this.props.token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch("https://localhost:44345/api/Document/add_document_type", fetchData)
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        this.handleOpenSnackbar(
          "Dodano kategorie o nazwie: " + this.state.name
        );
        this.setState({
          isLoading: false,
          name: "",
          verifyingUserAccount: ""
        });
      })
      .catch(error => {
        console.log(error);
        error.then(errorsKeyValueList => {
          this.setState({
            errorsBackend: handle400Errors(errorsKeyValueList),
            isLoading: false
          });
        });
      });
  }

  handleOpenSnackbar = snackbarMessage => {
    this.setState({ snackbarMessage, open: true });
  };

  handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    return (
      <DodajKategorieForm
        {...this.state}
        handleChange={this.handleChange}
        handleDirty={this.handleDirty}
        handleSubmit={this.handleSubmit}
        handleOpenSnackbar={this.handleOpenSnackbar}
        handleCloseSnackbar={this.handleCloseSnackbar}
      />
    );
  }
}

export default DodajKategorie;
