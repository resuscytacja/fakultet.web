import { Field, Schema } from "v4f";

export default Schema(
  {
    name: Field()
      .string()
      .alpha({ message: "Kategoria nie moze zawierac cyfr" })
      .max(40, { message: "Kategoria nie moze byc dluzsza niz niz 20 znakow" })
      .required({ message: "Kategoria jest wymagana" }),
    allowedToSendUserAccount: Field()
      .string()
      .required({ message: "Osoba mogąca wysyłać dokument jest wymagana" }),
    verifyingUserAccount: Field()
      .string()
      .required({ message: "Osoba weryfikująca dokument jest wymagana" }),
    acceptingUserAccount: Field()
      .string()
      .required({ message: "Osoba rozpatrująca dokument jest wymagana" })
  },
  { verbose: true, async: true }
);
