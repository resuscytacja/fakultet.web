import React, { Component } from "react";
import {
  withStyles,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Paper
} from "@material-ui/core/";
import TableSortableHeader from "./TableSortableHeader";

class ListaKategoriiTable extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableSortableHeader
            order={this.props.orderType}
            orderBy={this.props.orderBy}
            onRequestSort={this.props.handleRequestSort}
          />
          <TableBody>
            {this.props.documentTypeList.map(row => (
              <TableRow key={row.id}>
                <TableCell>
                  <strong>{row.name}</strong>
                </TableCell>
                <TableCell>{row.allowedToSendAccountJobPosition}</TableCell>
                <TableCell>{row.verifyingUserAccountJobPosition}</TableCell>
                <TableCell>{row.acceptingUserAccountJobPosition}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

const styles = theme => ({
  progress: {
    margin: theme.spacing(2),
    position: "absolute",
    left: "50%"
  },
  root: {
    width: "80%",
    marginTop: theme.spacing(3),
    overflowX: "auto",
    margin: "auto"
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  },
  button: { margin: 0 }
});

export default withStyles(styles)(ListaKategoriiTable);
