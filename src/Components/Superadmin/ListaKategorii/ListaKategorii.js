import React, { Component } from "react";
import ListaKategoriiTable from "./ListaKategoriiTable";

class ListaKategorii extends Component {
  _isMounted = false;

  state = {
    documentTypeList: [],
    orderBy: "Id",
    orderType: "desc"
  };

  componentDidMount() {
    this._isMounted = true;
    if (this.props.isLoggedIn) {
      this.getAllDocumentTypes(
        this.props.token,
        this.state.orderBy,
        this.state.orderType
      );
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
    if (nextProps.isLoggedIn) {
      this.getAllDocumentTypes(
        nextProps.token,
        this.state.orderBy,
        this.state.orderType
      );
    }
  }

  getAllDocumentTypes(token) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };
    fetch("https://localhost:44345/api/Document/all_document_types", fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({
            documentTypeList: data.documentTypeList
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let orderType = "desc";

    if (this.state.orderBy === property && this.state.orderType === "desc") {
      orderType = "asc";
    }

    this.setState({ orderType, orderBy });
    this.getAllDocumentTypes(this.props.token, orderBy, orderType);
  };

  render() {
    return (
      <ListaKategoriiTable
        {...this.props}
        {...this.state}
        handleRequestSort={this.handleRequestSort}
      />
    );
  }
}

export default ListaKategorii;
