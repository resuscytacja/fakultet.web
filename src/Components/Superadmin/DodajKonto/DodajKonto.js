import React, { Component } from "react";
import DodajKontoValidator from "./DodajKontoValidator";
import { handle400Errors } from "../../../Helpers/helpers";
import DodajKontoForm from "./DodajKontoForm";

class DodajKonto extends Component {
  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    firstName: "",
    lastName: "",
    email: "",
    numerIndeksu: "",
    role: "student", //  ustawiłem "student", żeby było coś wybrane na start w liście
    jobposition: "",
    errors: {},
    errorsBackend: "",
    open: false,
    snackbarMessage: "",
    isLoading: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.loggedInUserRole !== "superadmin") {
      this.props.history.push("/Home");
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDirty(e) {
    const { name, value } = e.target;

    const isValid = DodajKontoValidator[name].validate(value, {
      verbose: true,
      values: this.state
    });

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.register();
  }

  register() {
    this.setState({ isLoading: true, errorsBackend: "" });

    let accountToCreate = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      numerIndeksu: this.state.numerIndeksu,
      role: this.state.role,
      jobposition: ""
    };

    if (this.state.role === "moderator") {
      accountToCreate.jobposition = "pracownikdziekanatu";
    } else if (this.state.role === "student") {
      accountToCreate.jobposition = "student";
    } else {
      accountToCreate.jobposition = this.state.jobposition;
    }

    let fetchData = {
      method: "POST",
      body: JSON.stringify(accountToCreate),
      headers: {
        Authorization: "Bearer " + this.props.token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch("https://localhost:44345/api/SuperAdmin/add_account", fetchData)
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        console.log("Udalo sie");
        this.handleOpenSnackbar("Dodano konto o roli: " + this.state.role);
        this.setState({
          isLoading: false,
          firstName: "",
          lastName: "",
          email: "",
          numerIndeksu: "",
          jobposition: ""
        });
      })
      .catch(error => {
        console.log(error);
        error.then(errorsKeyValueList => {
          this.setState({
            errorsBackend: handle400Errors(errorsKeyValueList),
            isLoading: false
          });
        });
      });
  }

  handleOpenSnackbar = snackbarMessage => {
    this.setState({ snackbarMessage, open: true });
  };

  handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    return (
      <DodajKontoForm
        {...this.state}
        handleChange={this.handleChange}
        handleDirty={this.handleDirty}
        handleSubmit={this.handleSubmit}
        handleOpenSnackbar={this.handleOpenSnackbar}
        handleCloseSnackbar={this.handleCloseSnackbar}
      />
    );
  }
}

export default DodajKonto;
