import { Field, Schema } from "v4f";

export default Schema(
  {
    firstName: Field()
      .string()
      .alpha({ message: "Imie nie moze zawierac cyfr" })
      .max(20, { message: "Imie nie moze byc dluzsze niz 20 znakow" })
      .required({ message: "Imie jest wymagane" }),
    lastName: Field()
      .string()
      .alpha({ message: "Nazwisko nie moze zawierac cyfr" })
      .max(20, { message: "Nazwisko nie moze byc dluzsze niz 30 znakow" })
      .required({ message: "Nazwisko jest wymagane" }),
    email: Field()
      .string()
      .email({ message: "Podano nieprawidlowy adres email" })
      .required({ message: "Email jest wymagany" }),
    numerIndeksu: Field()
      .number()
      .max(6, { message: "Numer indeksu nie może być dłuższy niż 6 liczb" })
    // role: Field().required({ message: "Rola jest wymagana" })
  },
  { verbose: true, async: true }
);
