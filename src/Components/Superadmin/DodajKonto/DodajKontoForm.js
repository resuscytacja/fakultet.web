import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  Typography,
  withStyles,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Snackbar,
  CircularProgress
} from "@material-ui/core/";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

class DodajKontoForm extends Component {
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Dodaj nowe konto
          </Typography>
          <form className={classes.form}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Rola</InputLabel>
              <Select
                value={this.props.role}
                onChange={this.props.handleChange("role")}
                className={classes.selecto}
              >
                <MenuItem value="student">Student</MenuItem>
                <MenuItem value="admin">Admin</MenuItem>
                <MenuItem value="moderator">Moderator</MenuItem>
              </Select>
            </FormControl>

            <TextField
              id="firstName"
              label="Imie"
              name="firstName"
              value={this.props.firstName}
              onChange={this.props.handleChange("firstName")}
              onBlur={this.props.handleDirty}
              error={this.props.errors.firstName !== undefined}
              helperText={this.props.errors.firstName}
              fullWidth
              margin="normal"
              required
            />
            <TextField
              label="Nazwisko"
              id="lastName"
              name="lastName"
              value={this.props.lastName}
              onChange={this.props.handleChange("lastName")}
              onBlur={this.props.handleDirty}
              error={this.props.errors.lastName !== undefined}
              helperText={this.props.errors.lastName}
              fullWidth
              margin="normal"
              required
            />
            <TextField
              label="Email"
              id="email"
              name="email"
              type="email"
              value={this.props.email}
              onChange={this.props.handleChange("email")}
              onBlur={this.props.handleDirty}
              error={this.props.errors.email !== undefined}
              helperText={this.props.errors.email}
              fullWidth
              margin="normal"
              required
            />
            {this.props.role === "student" && (
              <TextField
                label="Numer Indeksu"
                id="numerIndeksu"
                name="numerIndeksu"
                type="number"
                value={this.props.numerIndeksu}
                onChange={this.props.handleChange("numerIndeksu")}
                onBlur={this.props.handleDirty}
                helperText={this.props.errors.Indeksu}
                fullWidth
                margin="normal"
                required
              />
            )}
            {this.props.role === "admin" && (
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="age-simple">Stanowisko</InputLabel>
                <Select
                  value={this.props.jobposition}
                  onChange={this.props.handleChange("jobposition")}
                  className={classes.selecto}
                >
                  <MenuItem value="dziekan">Dziekan</MenuItem>
                  <MenuItem value="rektor">Rektor</MenuItem>
                </Select>
              </FormControl>
            )}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.props.handleSubmit}
              disabled={this.props.isLoading}
            >
              Zarejestruj
            </Button>
            {this.props.isLoading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
            <p style={{ color: "red", whiteSpace: "pre-wrap" }}>
              {this.props.errorsBackend}
            </p>
          </form>
          <Snackbar
            anchorOrigin={{
              vertical: "top",
              horizontal: "right"
            }}
            open={this.props.open}
            autoHideDuration={3000}
            onClose={this.props.handleCloseSnackbar}
            ContentProps={{
              "aria-describedby": "message-id"
            }}
            message={<span id="message-id">{this.props.snackbarMessage}</span>}
            action={[
              <Button
                key="undo"
                color="inherit"
                size="small"
                onClick={this.props.handleCloseSnackbar}
              >
                Ok
              </Button>,
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={this.props.handleCloseSnackbar}
              >
                <CloseIcon />
              </IconButton>
            ]}
          />
        </Paper>
      </main>
    );
  }
}

const styles = theme => ({
  buttonProgress: {
    color: "green",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -24,
    marginLeft: -12
  },
  main: {
    width: "auto",
    display: "block",
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(6))]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  selecto: {
    width: "100%"
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1)
  },
  submit: {
    marginTop: theme.spacing(3)
  },
  formKontrol: 1000
});

export default withStyles(styles)(DodajKontoForm);
