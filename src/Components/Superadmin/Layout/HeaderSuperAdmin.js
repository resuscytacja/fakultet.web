import React, { Component } from "react";
import { withStyles } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import PowerSettingsNew from "@material-ui/icons/PowerSettingsNew";
import Add from "@material-ui/icons/Add";
import NoteAdd from "@material-ui/icons/NoteAdd";
import FormatListBulleted from "@material-ui/icons/FormatListBulleted";
import Settings from "@material-ui/icons/Settings";

class Header extends Component {
  state = {
    anchorEl: null
  };
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="secondary">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Fakultet - panel superadmina
            </Typography>
            <div className={classes.section}>
              <div className={classes.grow}>
                <Button
                  varinat="contained"
                  color="inherit"
                  className={classes.button}
                  data-test="listaKategorii"
                  onClick={() =>
                    this.props.history.push("/superadmin/listaKategorii")
                  }
                >
                  <FormatListBulleted />
                  Lista kategorii
                </Button>
                <Button
                  varinat="contained"
                  color="inherit"
                  className={classes.button}
                  data-test="dodajKategorie"
                  onClick={() =>
                    this.props.history.push("/superadmin/dodajKategorie")
                  }
                >
                  <NoteAdd />
                  Dodaj kategorię
                </Button>
                <Button
                  varinat="contained"
                  color="inherit"
                  className={classes.button}
                  data-test="dodajkonto"
                  onClick={() =>
                    this.props.history.push("/superadmin/dodajkonto")
                  }
                >
                  <Add />
                  Dodaj użytkownika
                </Button>
                <Button
                  varinat="contained"
                  color="inherit"
                  className={classes.button}
                  data-test="edycjaKonta"
                  onClick={() => this.props.history.push("/edycjaKonta")}
                >
                  <Settings />
                  Edycja konta
                </Button>
                <Button
                  varinat="contained"
                  color="inherit"
                  className={classes.button}
                  onClick={this.props.onLogout}
                >
                  <PowerSettingsNew />
                </Button>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  section: {
    justifyContent: "flex-end"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  button: {
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 5,
    padding: 10
  }
};
export default withStyles(styles)(Header);
