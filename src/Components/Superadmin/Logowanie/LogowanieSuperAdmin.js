import React, { Component } from "react";
import LogowanieSuperAdminForm from "./LogowanieSuperAdminForm";
import { handle400Errors } from "../../../Helpers/helpers";
import LogowanieSuperAdminValidator from "./LogowanieSuperAdminValidator";

class LogowanieSuperAdmin extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  state = {
    username: "",
    password: "",
    errors: {},
    errorsBackend: "",
    isLoading: false
  };

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.props.history.push("/superadmin/listaKategorii");
    }
  }

  handleChange = name => event => {
    if (this._isMounted) {
      this.setState({ [name]: event.target.value });
    }
  };

  handleDirty(e) {
    const { name, value } = e.target;

    const isValid = LogowanieSuperAdminValidator[name].validate(value, {
      verbose: true,
      values: this.props.state
    });

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    this.login();
  }

  login() {
    if (this._isMounted) {
      this.setState({ isLoading: true });
    }
    let accountToLogin = {
      username: this.state.username,
      password: this.state.password
    };

    let fetchData = {
      method: "POST",
      body: JSON.stringify(accountToLogin),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch("https://localhost:44345/api/SuperAdmin/login", fetchData)
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response.json();
      })
      .then(returnedData => {
        this.props.onLogin(returnedData.token);
        if (this._isMounted) {
          this.setState({ isLoading: false });
        }
      })
      .catch(error => {
        error.then(errorsKeyValueList => {
          this.setState({
            errorsBackend: handle400Errors(errorsKeyValueList),
            isLoading: false
          });
        });
      });
  }

  render() {
    return (
      <LogowanieSuperAdminForm
        {...this.state}
        {...this.props}
        handleChange={this.handleChange}
        handleDirty={this.handleDirty}
        handleSubmit={this.handleSubmit}
      />
    );
  }
}

export default LogowanieSuperAdmin;
