import { Field, Schema } from "v4f";

export default Schema(
  {
    username: Field().required({ message: "Nazwa użytkownika jest wymagana" }),
    password: Field()
      .string()
      .required({ message: "Hasło jest wymagane" })
  },
  { verbose: true, async: true }
);
