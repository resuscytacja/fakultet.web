import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  Typography,
  withStyles,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from "@material-ui/core/";
import Drafts from "@material-ui/icons/Drafts";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import { Link } from "react-router-dom";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
registerPlugin(FilePondPluginFileValidateType);

class WyslijDokumentForm extends Component {
  pond = {};

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <Drafts />
          </Avatar>
          <br />
          <Typography component="h1" variant="h5">
            Wyślij dokument
          </Typography>
          <br />
          <form className={classes.form} encType="multipart/form-data">
            <FormControl
              className={classes.category}
              variant="outlined"
              required
            >
              <InputLabel>Kategoria</InputLabel>
              <Select
                name="id"
                value={this.props.id}
                fullWidth
                onChange={this.props.handleChange("id")}
                onBlur={this.props.handleDirty}
                error={this.props.errors.id !== undefined}
              >
                {this.props.documentTypeList.map(id => (
                  <MenuItem key={id.id} value={id.id}>
                    {id.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <br />
            <TextField
              label="Tytuł"
              id="title"
              name="title"
              className={classes.title}
              onBlur={this.props.handleDirty}
              value={this.props.title}
              error={this.props.errors.title !== undefined}
              helperText={this.props.errors.title}
              onChange={this.props.handleChange("title")}
              fullWidth
              margin="normal"
              multiline
              rows="1"
              variant="outlined"
              required
            />
            <FilePond
              ref={ref => (this.pond = ref)}
              allowMultiple={true}
              acceptedFileTypes={[
                "application/pdf",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/msword",
                "application/vnd.oasis.opendocument.text"
              ]}
              labelIdle="Kliknij, aby dodać pliki lub upuść w ten obszar"
            />
            <Typography className={classes.formats}>
              Obsługiwane formaty to: *pdf *doc *docx *odt
            </Typography>

            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.props.handleSubmit(this.pond)}
            >
              Wyślij dokument
            </Button>
            <Link className={classes.link} to="/mojeDokumenty">
              <Button
                variant="contained"
                color="secondary"
                className={classes.submit}
              >
                Anuluj
              </Button>
            </Link>
            <Typography className={classes.redWarning}>
              {this.props.errorsBackend}
            </Typography>
          </form>
        </Paper>
      </main>
    );
  }
}

const styles = theme => ({
  main: {
    width: "auto",
    display: "block",
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
      height: 700
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 100
  },
  avatar: {
    marginTop: 10,
    backgroundColor: theme.palette.primary.main
  },
  category: {
    width: 450,
    display: "flex",
    justifyContent: "space-between"
  },
  formats: {
    textDecoration: "none",
    color: "grey",
    fontSize: "14px",
    alignItems: "center"
  },
  redWarning: {
    color: "red"
  },
  link: {
    textDecoration: "none"
  },
  submit: {
    marginBottom: 20,
    marginLeft: 28,
    width: 190
  }
});

export default withStyles(styles)(WyslijDokumentForm);
