import React, { Component } from "react";
import { handle400Errors } from "../../../Helpers/helpers";
import WyslijDokumentForm from "./WyslijDokumentForm";
import WyslijDokumentValidator from "./WyslijDokumentValidator";
class WyslijDokument extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getAllDocumentTypes = this.getAllDocumentTypes.bind(this);
  }

  state = {
    pond: {},
    documentTypeList: [],
    documentId: "",
    documentTypeName: "",
    status: "",
    fileList: [],
    name: "",
    id: "",
    errors: {},
    errorsBackend: ""
  };

  componentDidMount() {
    this._isMounted = true;
    if (this.props.isLoggedIn) {
      this.getAllDocumentTypes(this.props.token);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
    if (nextProps.isLoggedIn) {
      this.getAllDocumentTypes(nextProps.token);
    }
  }

  getAllDocumentTypes(token) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };
    fetch("https://localhost:44345/api/Document/all_document_types", fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({ documentTypeList: data.documentTypeList });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleSubmit = pond => event => {
    event.preventDefault();
    WyslijDokumentValidator.validate(this.state)
      .then(() => {
        this.submitDocument(pond);
      })
      .catch(errors => {
        this.setState({ errors });
      });
  };

  submitDocument(pond) {
    let formData = new FormData();
    formData.append("DocumentTypeId", this.state.id);
    formData.append("Title", this.state.title);

    pond
      .getFiles()
      .map(fileItem => fileItem.file)
      .forEach(file => {
        formData.append("FileList", file, file.name);
      });

    let fetchData = {
      body: formData,
      method: "POST",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch("https://localhost:44345/api/Document/add_document", fetchData)
      .then(response => {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then(() => {
        this.props.history.push("/mojeDokumenty");
      })
      .catch(error => {
        error.then(errorsKeyValueList => {
          this.setState({ errorsBackend: handle400Errors(errorsKeyValueList) });
        });
      });
  }

  testRef = ref => {
    this.setState({ pond: ref });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDirty(e) {
    const { name, value } = e.target;

    const isValid = WyslijDokumentValidator[name].validate(value, {
      verbose: true,
      values: this.props.state
    });

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  }

  render() {
    return (
      <WyslijDokumentForm
        {...this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        handleDirty={this.handleDirty}
        getAllDocumentTypes={this.getAllDocumentTypes}
        onDrop={this.onDrop}
        testRef={this.testRef}
      />
    );
  }
}

export default WyslijDokument;
