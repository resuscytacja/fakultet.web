import React, { Component } from "react";
import {
  withStyles,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Paper,
  Button,
  Typography
} from "@material-ui/core/";
import TableSortableHeader from "./TableSortableHeader";
import { Link } from "react-router-dom";
import BuildIcon from "@material-ui/icons/Build";

class ListaMoichDokumentowTable extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableSortableHeader
            order={this.props.orderType}
            orderBy={this.props.orderBy}
            onRequestSort={this.props.handleRequestSort}
          />
          <TableBody>
            {this.props.documents.map(row => (
              <TableRow key={row.documentId}>
                <TableCell>
                  {/* <Link
                    style={{
                      textDecoration: "none",
                      color: "blue",
                      fontSize: "14px",
                      alignItems: "left"
                    }}
                    to={"/dokumenty/wyswietl/" + row.documentId}
                  > */}
                  {/* Jest w ten sposob, bo inaczej byl blad, ze <Link> nie powinien byc uzywany poza <Router> */}
                  <strong
                    style={{
                      textDecoration: "none",
                      color: "blue",
                      fontSize: "14px",
                      alignItems: "left"
                    }}
                    onClick={() =>
                      this.props.history.push(
                        "/dokumenty/edytuj/" + row.documentId
                      )
                    }
                  >
                    {row.title}
                  </strong>
                  {/* </Link> */}
                </TableCell>
                <TableCell>{row.documentTypeName}</TableCell>
                <TableCell>
                  {row.fileList !== undefined &&
                    row.fileList.map(file => (
                      <Typography className={classes.pliki} key={file}>
                        {file}
                        <br />
                      </Typography>
                    ))}
                </TableCell>
                <TableCell>{row.sendTime.substring(0, 10)}</TableCell>
                <TableCell>{row.status}</TableCell>
                {this.props.loggedInUserRole === "Student" && (
                  <TableCell>
                    {/* <Link
                      style={{
                        textDecoration: "none",
                        color: "black",
                        padding: "10px",
                        margin: "10px",
                        flexDirection: "row",
                        justifyContent: "flex-end",
                        fontWeight: "bold"
                      }}
                      to={"/dokumenty/edytuj/" + row.documentId}
                    > */}
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      onClick={() =>
                        this.props.history.push(
                          "/dokumenty/edytuj/" + row.documentId
                        )
                      }
                    >
                      Edytuj
                      <BuildIcon
                        className={(classes.leftIcon, classes.iconSmall)}
                      />
                    </Button>
                    {/* </Link> */}
                  </TableCell>
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

const styles = theme => ({
  progress: {
    margin: theme.spacing(2),
    position: "absolute",
    left: "50%"
  },
  root: {
    width: "80%",
    marginTop: theme.spacing(3),
    overflowX: "auto",
    margin: "auto"
  },
  row: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  },
  button: { margin: "auto" }
});

export default withStyles(styles)(ListaMoichDokumentowTable);
