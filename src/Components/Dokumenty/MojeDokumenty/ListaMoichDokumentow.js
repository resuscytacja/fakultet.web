import React, { Component } from "react";
import ListaMoichDokumentowTable from "./ListaMoichDokumentowTable";

class ListaMoichDokumentow extends Component {
  _isMounted = false;

  state = {
    documents: [],
    orderBy: "Id",
    orderType: "desc"
  };

  componentDidMount() {
    this._isMounted = true;
    if (this.props.isLoggedIn) {
      this.displayMyDocuments(
        this.props.token,
        this.state.orderBy,
        this.state.orderType
      );
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
    if (nextProps.isLoggedIn) {
      this.displayMyDocuments(
        nextProps.token,
        this.state.orderBy,
        this.state.orderType
      );
    }
  }

  displayMyDocuments(token, orderBy, orderType) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    };

    let apiUrl = "https://localhost:44345/api/Document/documentList?";
    let orderByParam = "orderBy=" + orderBy;
    let orderTypeParam = "orderType=" + orderType;
    apiUrl += orderByParam + "&" + orderTypeParam;

    fetch(apiUrl, fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({
            documents: data.documents.documents
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let orderType = "desc";

    if (this.state.orderBy === property && this.state.orderType === "desc") {
      orderType = "asc";
    }

    this.setState({ orderType, orderBy });
    this.displayMyDocuments(this.props.token, orderBy, orderType);
  };

  render() {
    console.log(this.state.documents);
    return (
      <ListaMoichDokumentowTable
        {...this.props}
        {...this.state}
        handleRequestSort={this.handleRequestSort}
      />
    );
  }
}

export default ListaMoichDokumentow;
