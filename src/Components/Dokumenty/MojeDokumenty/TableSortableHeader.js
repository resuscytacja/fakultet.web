import React, { Component } from "react";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Tooltip from "@material-ui/core/Tooltip";

const tableHeadRows = [
  {
    id: "title",
    numeric: false,
    disablePadding: false,
    label: "Tytuł"
  },
  {
    id: "documentTypeName",
    numeric: false,
    disablePadding: false,
    label: "Kategoria"
  },
  {
    id: "fileSend",
    numeric: false,
    disablePadding: false,
    label: "Załączniki"
  },
  {
    id: "sendTime",
    numeric: false,
    disablePadding: false,
    label: "Data wysłania"
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Status"
  }
];

class TableSortableHeader extends Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  render() {
    const { order, orderBy } = this.props;

    return (
      <TableHead>
        <TableRow>
          {tableHeadRows.map(
            row => (
              <TableCell
                key={row.id}
                align={row.numeric ? "right" : "left"}
                padding={row.disablePadding ? "none" : "default"}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={this.createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

export default TableSortableHeader;
