import { Field, Schema } from "v4f";
export default Schema(
  {
    id: Field().required({ message: "Kategoria jest wymagana" }),
    title: Field()
      .string()
      .max(50, { message: "Tytuł nie może przekraczać 50 znaków" })
      .min(3, { message: "Tytuł musi być dłuższy niż 3 znaki" })
      .required({ message: "Tytuł jest wymagany" }),
    documentTypeName: Field().required({ message: "Kategoria jest wymagana" })
  },
  { verbose: true, async: true }
);
