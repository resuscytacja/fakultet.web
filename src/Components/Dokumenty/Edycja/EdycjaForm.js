import React, { Component } from "react";
import {
  Avatar,
  Button,
  CssBaseline,
  Paper,
  Typography,
  withStyles,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from "@material-ui/core/";
import Drafts from "@material-ui/icons/Drafts";
import DeleteIcon from "@material-ui/icons/Delete";
import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";

registerPlugin(FilePondPluginFileValidateType);

class EdycjaForm extends Component {
  pond = {};
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <Drafts />
          </Avatar>
          <br />
          <Typography component="h1" variant="h5">
            Edytuj dokument
          </Typography>
          <br />
          <form className={classes.form} encType="multipart/form-data">
            <FormControl
              className={classes.category}
              variant="outlined"
              required
            >
              <InputLabel>Kategoria</InputLabel>
              <Select
                name="id"
                fullWidth
                value={this.props.id}
                onChange={this.props.handleChange("id")}
                error={this.props.errors.id !== undefined}
              >
                {this.props.documentTypeList.map(id => (
                  <MenuItem key={id.id} value={id.id}>
                    {id.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <br />
            <TextField
              label="Tytuł"
              id="title"
              name="title"
              className={classes.title}
              value={this.props.title}
              error={this.props.errors.title !== undefined}
              helperText={this.props.errors.title}
              onChange={this.props.handleChange("title")}
              fullWidth
              margin="normal"
              multiline
              rows="1"
              variant="outlined"
              required
            />

            <div className={classes.container}>
              <Typography>Wysłane pliki:</Typography>
              {this.props.fileList !== undefined &&
                this.props.fileList.map(file => {
                  return (
                    <div key={file} className={classes.wyswietlacz}>
                      <Button
                        variant="contained"
                        color="secondary"
                        className={classes.button}
                        data-test="fileDeleteButton"
                        onClick={() => this.props.handleFileDelete(file)}
                      >
                        <DeleteIcon className={classes.rightIcon} />
                      </Button>
                      <a
                        href={"https://localhost:44345/file/" + file}
                        target="blank"
                        key={file}
                      >
                        {file}
                      </a>
                    </div>
                  );
                })}
            </div>
            <FilePond
              ref={ref => (this.pond = ref)}
              allowMultiple={true}
              acceptedFileTypes={[
                "application/pdf",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/msword",
                "application/vnd.oasis.opendocument.text"
              ]}
              labelIdle="Kliknij, aby dodać pliki lub upuść w ten obszar"
            />
            <Typography className={classes.formats}>
              Obsługiwane formaty to: *pdf *doc *docx *odt
            </Typography>

            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.props.handleEdit(this.pond)}
            >
              Zapisz
            </Button>
            <Button
              variant="contained"
              color="secondary"
              className={classes.submit}
              onClick={() => this.props.history.push("/mojeDokumenty")}
            >
              Anuluj
            </Button>
            <p style={{ color: "red", whiteSpace: "pre-wrap" }}>
              {this.props.errorsBackend}
            </p>
          </form>
        </Paper>
      </main>
    );
  }
}

const styles = theme => ({
  main: {
    width: "auto",
    display: "block",
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
      height: 700
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 100
  },
  avatar: {
    marginTop: 10,
    backgroundColor: theme.palette.primary.main
  },
  category: {
    width: 450,
    display: "flex",
    justifyContent: "space-between"
  },
  submit: {
    marginBottom: 20,
    marginLeft: 65,
    width: 120
  },
  formats: {
    textDecoration: "none",
    color: "grey",
    fontSize: "14px",
    alignItems: "center"
  },
  wyswietlacz: {
    paddingTop: 5,
    paddingBottom: 5
  },
  link: {
    textDecoration: "none"
  }
});

export default withStyles(styles)(EdycjaForm);
