import React, { Component } from "react";
import { handle400Errors } from "../../../Helpers/helpers";
import EdycjaForm from "./EdycjaForm";
import EdycjaValidator from "./EdycjaValidator";

class Edycja extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.handleDirty = this.handleDirty.bind(this);
    this.getAllDocumentTypes = this.getAllDocumentTypes.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleExit = this.handleExit.bind(this);
  }

  state = {
    pond: {},
    documentTypeList: [],
    documentId: "",
    documentTypeName: "",
    status: "",
    fileList: [],
    removeFiles: [],
    name: "",
    title: "",
    id: "",
    errors: {},
    errorsBackend: ""
  };

  componentDidMount() {
    this._isMounted = true;

    const {
      match: { params }
    } = this.props;
    if (this.props.isLoggedIn) {
      this.getDocument(this.props.token, params.docId);
      this.getAllDocumentTypes(this.props.token);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      match: { params }
    } = this.props;

    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
    if (nextProps.isLoggedIn) {
      this.getDocument(nextProps.token, params.docId);
      this.getAllDocumentTypes(nextProps.token);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getAllDocumentTypes(token) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };
    fetch("https://localhost:44345/api/Document/all_document_types", fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({ documentTypeList: data.documentTypeList });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  testRef = ref => {
    this.setState({ pond: ref });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDirty(e) {
    const { name, value } = e.target;

    const isValid = EdycjaValidator[name].validate(value, {
      verbose: true,
      values: this.props.state
    });

    if (isValid !== true) {
      this.setState({
        errors: { ...this.state.errors, [name]: isValid }
      });
    } else {
      this.setState({
        errors: { ...this.state.errors, [name]: undefined }
      });
    }
  }

  getDocument(token, docId) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    };
    this.setState({ isLoading: true });

    fetch("https://localhost:44345/api/Document/" + docId, fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({
            title: data.title,
            documentTypeName: data.documentTypeName,
            fileList: data.fileList,
            isLoading: false,
            id: data.documentTypeId
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleFileDelete = fileToRemove => {
    let updatedImagesList = this.state.fileList.filter(
      file => file !== fileToRemove
    );
    this.setState({
      removeFiles: [...this.state.removeFiles, fileToRemove],
      fileList: updatedImagesList
    });
  };

  handleEdit = pond => event => {
    event.preventDefault();

    const {
      match: { params }
    } = this.props;

    this.submitDocument(pond, params.docId);
  };

  submitDocument(pond, docId) {
    let formData = new FormData();
    formData.append("DocumentTypeId", this.state.id);
    formData.append("Title", this.state.title);
    if (this.state.removeFiles.length !== 0) {
      formData.append("removeFile", this.state.removeFiles);
    }

    pond
      .getFiles()
      .map(fileItem => fileItem.file)
      .forEach(file => {
        formData.append("FileList", file, file.name);
      });

    let fetchData = {
      body: formData,
      method: "POST",
      headers: {
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch("https://localhost:44345/api/Document/edit/" + docId, fetchData)
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response.json();
      })
      .then(() => {
        this.props.history.push("/mojeDokumenty");
      })
      .catch(error => {
        error.then(errorsKeyValueList => {
          this.setState({ errorsBackend: handle400Errors(errorsKeyValueList) });
        });
      });
  }

  handleExit = () => {
    this.history.push("/mojeDokumenty");
  };

  render() {
    return (
      <EdycjaForm
        {...this.state}
        handleChange={this.handleChange}
        handleDirty={this.handleDirty}
        getAllDocumentTypes={this.getAllDocumentTypes}
        onDrop={this.onDrop}
        testRef={this.testRef}
        handleFileDelete={this.handleFileDelete}
        handleEdit={this.handleEdit}
        handleExit={this.handleExit}
      />
    );
  }
}

export default Edycja;
