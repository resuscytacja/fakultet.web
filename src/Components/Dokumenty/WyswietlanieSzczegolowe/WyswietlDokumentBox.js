import React, { Component, Fragment } from "react";
import {
  withStyles,
  Paper,
  Typography,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  TextField
} from "@material-ui/core";
import DescriptionIcon from "@material-ui/icons/Description";

class WyswietlDokumentBox extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <Paper className={classes.paper}>
          <Typography className={classes.category}>
            <DescriptionIcon />

            {this.props.document.documentTypeName}
          </Typography>
          <div className={classes.text}>
            <Typography className={classes.temat}>
              Temat dokumentu: <strong>{this.props.document.title}</strong>
            </Typography>
          </div>
          <div>
            <Typography className={classes.temat}>
              Wysłano do:
              <strong>{this.props.document.documentRecipient}</strong>
            </Typography>
          </div>
          <div className={classes.container}>
            <div>
              <Typography>Wysłane pliki:</Typography>

              <Typography className={classes.pliki}>
                {this.props.document.fileList !== undefined &&
                  this.props.document.fileList.map(file => (
                    <a
                      href={"https://localhost:44345/file/" + file}
                      target="blank"
                      key={file}
                    >
                      {file}
                      <br />
                    </a>
                  ))}
              </Typography>
            </div>
            {this.props.document.sendTime !== undefined && (
              <Typography>
                Przesłano: {this.props.document.sendTime.slice(0, 10)}
              </Typography>
            )}
          </div>
          <div className={classes.container}>
            {this.props.document.status === "Awaiting" && (
              <Typography className={classes.awaiting}>
                Oczekujące na zatwierdzenie
              </Typography>
            )}
            {this.props.document.status === "Verified" && (
              <Typography className={classes.verified}>
                Zatwierdzone przez dziekanat
              </Typography>
            )}
            {this.props.document.status === "Unverified" && (
              <Typography className={classes.rejected}>
                Odrzucone przez dziekanat
              </Typography>
            )}
            {this.props.document.status === "Accepted" && (
              <Typography className={classes.confirmed}>
                Zatwierdzone
              </Typography>
            )}
            {this.props.document.status === "Cancelled" && (
              <Typography className={classes.anulowane}>Odrzucone!</Typography>
            )}
            {this.props.document.status === "Return" && (
              <Typography className={classes.returned}>
                Zwrócone do dziekanatu
              </Typography>
            )}
          </div>
          {this.props.document.rejectedReason !== undefined && (
            <div>
              <Typography>
                Powód odrzucenia: {this.props.document.rejectedReason}
              </Typography>
              <Typography>
                Czas rozpatrzenia: {this.props.document.resolveTime}
              </Typography>
            </div>
          )}
          {this.props.document.status === "Awaiting" &&
            this.props.loggedInUserRole === "Moderator" && (
              <div className={classes.container2}>
                <Button
                  className={classes.akceptejszyn}
                  onClick={() => this.props.handleConfirm()}
                >
                  Przekaż dalej
                </Button>
                <Dialog
                  open={this.props.openFirst}
                  onClose={this.props.handleConfirmClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">
                    Potwierdzanie
                  </DialogTitle>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleModeratorVerify}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                    <Button
                      onClick={this.props.handleConfirmClose}
                      color="secondary"
                    >
                      Odrzuć
                    </Button>
                  </DialogActions>
                </Dialog>
                <Button
                  className={classes.odrzucejszyn}
                  onClick={() => this.props.handleClickOpen()}
                >
                  Odrzuć
                </Button>
                <Dialog
                  open={this.props.open}
                  onClose={this.props.handleClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">Odrzucanie</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Podaj powód odrzucenia
                    </DialogContentText>
                    <TextField
                      id="powod"
                      label="Powód"
                      name="reason"
                      value={this.props.reason}
                      autoFocus
                      margin="dense"
                      onChange={this.props.handleChange("reason")}
                      fullWidth
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleModeratorVerify}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                  </DialogActions>
                </Dialog>
              </div>
            )}

          {this.props.document.status === "Verified" &&
            this.props.loggedInUserRole === "Admin" && (
              <div className={classes.container2}>
                <Button
                  className={classes.zatwierdz}
                  onClick={() => this.props.handleAdminConfirmOpen()}
                >
                  Zatwierdź
                </Button>
                <Dialog
                  open={this.props.openAdminConfirm}
                  onClose={this.props.handleAdminConfirmClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">
                    Zatwierdzanie
                  </DialogTitle>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleAdminDecision}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                    <Button
                      onClick={this.props.handleAdminConfirmClose}
                      color="secondary"
                    >
                      Odrzuć
                    </Button>
                  </DialogActions>
                </Dialog>
                <Button
                  className={classes.anuluj}
                  onClick={() => this.props.handleAdminCancelOpen()}
                >
                  Odrzuć
                </Button>
                <Dialog
                  open={this.props.openAdminCancel}
                  onClose={this.props.handleAdminCancelClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">Odrzucanie</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Podaj powód odrzucenia
                    </DialogContentText>
                    <TextField
                      id="powod"
                      label="Powód"
                      name="reason"
                      value={this.props.reasonAdmin}
                      autoFocus
                      margin="dense"
                      onChange={this.props.handleChange("reasonAdmin")}
                      fullWidth
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleAdminDecision}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                  </DialogActions>
                </Dialog>
                <Button
                  className={classes.return}
                  onClick={() => this.props.handleAdminReturnOpen()}
                >
                  Przekaż do dziekanatu
                </Button>
                <Dialog
                  open={this.props.openAdminReturn}
                  onClose={this.props.handleAdminReturnClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">
                    Przekazanie do dziekanatu
                  </DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Podaj powód przekazania do dziekanatu
                    </DialogContentText>
                    <TextField
                      id="powodAdmin"
                      label="Powód"
                      name="reasonAdmin"
                      value={this.props.reasonAdmin}
                      autoFocus
                      margin="dense"
                      onChange={this.props.handleChange("reasonAdmin")}
                      fullWidth
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleAdminDecision}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                  </DialogActions>
                </Dialog>
              </div>
            )}
          {this.props.document.status === "Return" &&
            this.props.loggedInUserRole === "Moderator" && (
              <div className={classes.container2}>
                <Button
                  className={classes.akceptejszyn}
                  onClick={() => this.props.handleConfirm()}
                >
                  Przekaż dalej
                </Button>
                <Dialog
                  open={this.props.openFirst}
                  onClose={this.props.handleConfirmClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">
                    Potwierdzanie
                  </DialogTitle>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleModeratorVerify}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                    <Button
                      onClick={this.props.handleConfirmClose}
                      color="secondary"
                    >
                      Odrzuć
                    </Button>
                  </DialogActions>
                </Dialog>
                <Button
                  className={classes.odrzucejszyn}
                  onClick={() => this.props.handleClickOpen()}
                >
                  Odrzuć
                </Button>
                <Dialog
                  open={this.props.open}
                  onClose={this.props.handleClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">Odrzucanie</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Podaj powód odrzucenia
                    </DialogContentText>
                    <TextField
                      id="powod"
                      label="Powód"
                      name="reason"
                      value={this.props.reason}
                      autoFocus
                      margin="dense"
                      onChange={this.props.handleChange("reason")}
                      fullWidth
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={this.props.handleModeratorVerify}
                      color="primary"
                    >
                      Zatwierdź
                    </Button>
                  </DialogActions>
                </Dialog>
              </div>
            )}
          <Typography>
            <strong>Historia dokumentu:</strong>
          </Typography>
          <hr />
          {this.props.activities !== undefined &&
            this.props.activities.map(row => (
              <div key={row.dateTime} className={classes.container3}>
                <Typography className={classes.history} key={row.dateTime}>
                  {row.message}
                </Typography>
                <Typography className={classes.history}>
                  {row.dateTime.slice(0, 10)} {row.dateTime.slice(11, 16)}
                </Typography>
              </div>
            ))}
          <hr />
        </Paper>
      </Fragment>
    );
  }
}

const styles = theme => ({
  paper: {
    display: "block",
    [theme.breakpoints.up(400 + theme.spacing(6))]: {
      width: "50%",
      marginTop: "2%",
      marginBottom: "2%",
      marginLeft: "20%",
      marginRight: "20%",
      backgroundColor: "#eeeeee"
    },
    flexGrow: 1,
    paddingBottom: "2%",
    paddingRight: "4%",
    paddingLeft: "4%",
    paddingTop: "1%"
  },
  category: {
    textAlign: "right",
    padding: 0,
    margin: 0,
    textDecorationLine: "underline",
    textDecorationColor: "blue",
    textDecorationStyle: "solid",
    fontSize: 20
  },
  awaiting: {
    backgroundColor: "#eeff41",
    padding: 10,
    borderRadius: 5
  },
  confirmed: {
    backgroundColor: "green",
    padding: 10,
    borderRadius: 5,
    color: "white"
  },
  anulowane: {
    backgroundColor: "red",
    padding: 10,
    borderRadius: 5,
    color: "white"
  },
  returned: {
    backgroundColor: "orange",
    padding: 10,
    borderRadius: 5
  },
  verified: {
    backgroundColor: "#9ccc65",
    padding: 10,
    borderRadius: 5
  },
  rejected: {
    backgroundColor: "#e57373",
    padding: 10,
    borderRadius: 5
  },
  text: {
    marginTop: "2%"
  },
  button: {
    backgroundColor: "#90caf9"
  },
  ikonka: {
    marginTop: 10
  },
  temat: {
    fontSize: 20,
    marginTop: "2%",
    marginBottom: "2%"
  },
  pliki: {
    color: "blue",
    fontSize: 16
  },
  temat2: {
    marginLeft: "22%",
    marginRight: "22%",
    fontSize: 22,
    textAlign: "center"
  },
  history: {
    fontSize: 12,
    padding: 10
  },
  docname: {
    marginLeft: "2%"
  },
  text2: {
    marginTop: "4%",
    marginBottom: "4%"
  },
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "3%"
  },
  container2: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "3%",
    marginTop: "5%"
  },
  container3: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 0
  },
  akceptejszyn: {
    marginLeft: "25%",
    backgroundColor: "#43a047",
    color: "white",
    padding: 20,
    width: 160
  },
  odrzucejszyn: {
    marginRight: "25%",
    backgroundColor: "#e53935",
    color: "white",
    padding: 20,
    width: 160
  },
  zatwierdz: {
    backgroundColor: "green",
    padding: 20,
    width: 200
  },
  anuluj: {
    backgroundColor: "red",
    padding: 20,
    width: 200
  },
  return: {
    backgroundColor: "orange",
    padding: 20
  }
});

export default withStyles(styles)(WyswietlDokumentBox);
