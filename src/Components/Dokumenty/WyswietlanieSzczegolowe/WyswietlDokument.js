import React, { Component } from "react";
import WyswietlDokumentBox from "./WyswietlDokumentBox";

class WyswietlDokument extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKlikacz = this.handleKlikacz.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleConfirmClose = this.handleConfirmClose.bind(this);
    this.handleModeratorVerify = this.handleModeratorVerify.bind(this);
    this.handleAdminConfirmOpen = this.handleAdminConfirmOpen.bind(this);
    this.handleAdminConfirmClose = this.handleAdminConfirmClose.bind(this);
    this.handleAdminCancelOpen = this.handleAdminCancelOpen.bind(this);
    this.handleAdminCancelClose = this.handleAdminCancelClose.bind(this);
    this.handleAdminReturnOpen = this.handleAdminReturnOpen.bind(this);
    this.handleAdminReturnClose = this.handleAdminReturnClose.bind(this);
    this.handleAdminDecision = this.handleAdminDecision.bind(this);
  }

  state = {
    document: {},
    documentId: "",
    name: "",
    recipientJobPosition: "",
    open: false,
    openFirst: false,
    openAdminConfirm: false,
    openAdminCancel: false,
    openAdminReturn: false,
    setOpen: false,
    errorsBackend: "",
    isLoading: true,
    klikacz: false,
    status: "",
    reason: "",
    reasonAdmin: "",
    activities: []
  };

  componentDidMount() {
    this._isMounted = true;

    const {
      match: { params }
    } = this.props;
    if (this.props.isLoggedIn) {
      this.getDocument(this.props.token, params.docId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      match: { params }
    } = nextProps;
    if (!nextProps.isLoggedIn) {
      this.props.history.push("/Home");
    }
    if (nextProps.isLoggedIn) {
      this.getDocument(nextProps.token, params.docId);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getDocument(token, docId) {
    let fetchData = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    };
    this.setState({ isLoading: true });

    fetch("https://localhost:44345/api/Document/" + docId, fetchData)
      .then(resp => resp.json())
      .then(data => {
        if (this._isMounted) {
          this.setState({
            document: data,
            activities: data.documentActivitiesList,
            isLoading: false
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleModeratorVerify() {
    let statusToSend = {
      status: this.state.status,
      reason: this.state.reason
    };

    let fetchData = {
      method: "PUT",
      body: JSON.stringify(statusToSend),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch(
      "https://localhost:44345/api/Document/verification/" +
        this.state.document.documentId,
      fetchData
    )
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        this.props.history.push("/mojeDokumenty");
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleAdminDecision() {
    let statusToSend = {
      status: this.state.status,
      reason: this.state.reasonAdmin
    };

    let fetchData = {
      method: "PUT",
      body: JSON.stringify(statusToSend),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token
      }
    };

    fetch(
      "https://localhost:44345/api/Document/consideration/" +
        this.state.document.documentId,
      fetchData
    )
      .then(response => {
        if (!response.ok) {
          throw response.json();
        }
        return response;
      })
      .then(() => {
        this.props.history.push("/mojeDokumenty");
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleClose = () => {
    this.setState({ open: false, status: "" });
  };

  handleClickOpen = () => {
    this.setState({ open: true, status: "UnVerified" });
  };

  handleConfirm = () => {
    this.setState({ openFirst: true, status: "Verified" });
  };
  handleConfirmClose = () => {
    this.setState({ openFirst: false, status: "" });
  };

  handleAdminConfirmOpen = () => {
    this.setState({ openAdminConfirm: true, status: "Accepted" });
  };

  handleAdminConfirmClose = () => {
    this.setState({ openAdminConfirm: false, status: "" });
  };

  handleAdminCancelOpen = () => {
    this.setState({ openAdminCancel: true, status: "Cancelled" });
  };

  handleAdminCancelClose = () => {
    this.setState({ openAdminCancel: false, status: "" });
  };

  handleAdminReturnOpen = () => {
    this.setState({ openAdminReturn: true, status: "Return" });
  };

  handleAdminReturnClose = () => {
    this.setState({ openAdminReturn: false, status: "" });
  };

  handleKlikacz = () => {
    this.setState({ klikacz: true });
  };

  handleOpenSnackbar = snackbarMessage => {};

  handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
  };

  render() {
    console.log(this.state.reasonAdmin);
    return (
      <WyswietlDokumentBox
        {...this.state}
        {...this.props}
        handleChange={this.handleChange}
        handleClickOpen={this.handleClickOpen}
        handleClose={this.handleClose}
        handleKlikacz={this.handleKlikacz}
        handleConfirm={this.handleConfirm}
        handleConfirmClose={this.handleConfirmClose}
        handleModeratorVerify={this.handleModeratorVerify}
        handleAdminConfirmOpen={this.handleAdminConfirmOpen}
        handleAdminConfirmClose={this.handleAdminConfirmClose}
        handleAdminCancelOpen={this.handleAdminCancelOpen}
        handleAdminCancelClose={this.handleAdminCancelClose}
        handleAdminReturnOpen={this.handleAdminReturnOpen}
        handleAdminReturnClose={this.handleAdminReturnClose}
        handleAdminDecision={this.handleAdminDecision}
      />
    );
  }
}
export default WyswietlDokument;
